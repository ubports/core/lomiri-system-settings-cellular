/*
 * This file is part of system-settings
 *
 * Copyright (C) 2014 Canonical Ltd.
 *
 * Contact: Pat McGowan <pat.mcgowan@canonical.com>,
 *          Jonas G. Drange <jonas.drange@canonical.com>
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 3, as published
 * by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranties of
 * MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR
 * PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.4
import SystemSettings 1.0
import Lomiri.Components 1.3
import Lomiri.Components.Popups 1.3
import Lomiri.Components.ListItems 1.3 as ListItem
import QOfono 0.2
import "Components" as LocalComponents
import "apn_editor.js" as Editor
import "i18nd.js" as I18nd

ItemPage {
    id: root
    objectName: "apnEditor"

    property OfonoContextConnection contextQML

    // All models.
    property ListModel mmsModel
    property ListModel internetModel
    property ListModel iaModel

    // Flags that indicate what context we are editing.
    property bool isCombo: Editor.indexToType(typeSel.selectedIndex) === 'internet+mms'
    property bool isInternet: Editor.indexToType(typeSel.selectedIndex) === 'internet'
    property bool isMms: Editor.indexToType(typeSel.selectedIndex) === 'mms'
    property bool isIa: Editor.indexToType(typeSel.selectedIndex) === 'ia'

    property bool isValid: Editor.isValid()

    // priv
    property bool _edgeReady: false

    property QtObject activeItem: null

    // When a user has requested saving a context.
    signal saving ()

    // When user has saved a context.
    signal saved ()

    // Signal for new contexts seen by the manger.
    signal newContext (OfonoContextConnection context)

    signal requestContextCreation (string type)

    // When user cancels.
    signal canceled ()

    header: PageHeader {
        id: chooseApnHeader
        title: contextQML ? I18nd.tr("Edit") : I18nd.tr("New APN")
        flickable: scrollArea
        trailingActionBar {
            actions: [
                Action {
                    objectName: "saveApn"
                    iconName: "ok"
                    enabled: isValid && state != "busy"
                    onTriggered: Editor.saving()
                }
            ]
        }
    }

    onSaving: state = "busy"
    onSaved: pageStack.removePages(root);
    onCanceled: pageStack.removePages(root);
    onNewContext: Editor.newContext(context);

    Component.onCompleted: {
        if (contextQML) {
            Editor.populate(contextQML);
        }
        Editor.ready();
    }

    // We need to disable keyboard anchoring because we implement the
    // KeyboardRectangle pattern
    Binding {
        target: main
        property: "anchorToKeyboard"
        value: false
    }

    Flickable {
        id: scrollArea
        objectName: "scrollArea"

        // this is necessary to avoid the page to appear below the header
        clip: true
        flickableDirection: Flickable.VerticalFlick
        anchors {
            fill: parent
            bottomMargin: keyboard.height
        }
        contentHeight: contents.height + units.gu(2)
        contentWidth: parent.width

        // after add a new field we need to wait for the contentHeight to
        // change to scroll to the correct position
        onContentHeightChanged: Editor.makeMeVisible(root.activeItem)

        Column {
            id: contents
            anchors { left: parent.left; right: parent.right; }

            // Type controls
            Column {
                anchors { left: parent.left; right: parent.right; }

                SettingsItemTitle {
                    text: I18nd.tr("Used for:")
                }

                ListItem.ItemSelector {
                    model: [I18nd.tr("Internet and MMS"),
                            I18nd.tr("Internet"),
                            I18nd.tr("MMS"),
                            I18nd.tr("LTE"), ]
                    id: typeSel
                    objectName: "typeSelector"
                    delegate: OptionSelectorDelegate {
                        showDivider: false
                        objectName: "type_" + Editor.indexToType(index)
                    }
                    width: parent.width
                    KeyNavigation.tab: name
                }
            }

            // Name controls
            Column {
                anchors { left: parent.left; right: parent.right; }

                SettingsItemTitle {
                    anchors { left: parent.left; right: parent.right }
                    text: I18nd.tr("Name")
                }

                LocalComponents.LabelTextField {
                    id: name
                    enabled: state != "busy"
                    objectName: "name"
                    inputMethodHints: Qt.ImhNoAutoUppercase |
                                      Qt.ImhNoPredictiveText
                    placeholderText: I18nd.tr("Enter a name describing the APN")
                    next: accessPointName
                    Component.onCompleted: forceActiveFocus()
                    onActiveFocusChanged: if (activeFocus) Editor.makeMeVisible(
                        name)
                }
            }

            // APN controls
            Column {
                anchors { left: parent.left; right: parent.right; }

                SettingsItemTitle {
                    anchors { left: parent.left; right: parent.right }
                    /* TRANSLATORS: This string is a description of a text
                    field and should thus be concise. */
                    text: I18nd.tr("APN")
                }

                LocalComponents.LabelTextField {
                    id: accessPointName
                    enabled: state != "busy"
                    objectName: "accessPointName"
                    inputMethodHints: Qt.ImhUrlCharactersOnly |
                                      Qt.ImhNoAutoUppercase |
                                      Qt.ImhNoPredictiveText
                    placeholderText: I18nd.tr("Enter the name of the access point")
                    next: isMms || isCombo ? messageCenter : username
                    onActiveFocusChanged: if (activeFocus) Editor.makeMeVisible(
                        accessPointName)
                }
            }

            // MMSC controls
            Column {
                anchors { left: parent.left; right: parent.right }
                visible: isMms || isCombo

                SettingsItemTitle {
                    anchors { left: parent.left; right: parent.right }
                    text: I18nd.tr("MMSC")
                }

                LocalComponents.LabelTextField {
                    id: messageCenter
                    enabled: state != "busy"
                    objectName: "messageCenter"
                    inputMethodHints: Qt.ImhUrlCharactersOnly |
                                      Qt.ImhNoAutoUppercase |
                                      Qt.ImhNoPredictiveText
                    placeholderText: I18nd.tr("Enter message center")
                    next: messageProxy
                    onFocusChanged: {
                        if (!focus && text.length > 0) {
                            text = Editor.setHttp(text);
                        }
                        if (activeFocus) Editor.makeMeVisible(messageCenter);
                    }
                }
            }

            // Proxy controls
            Column {
                anchors { left: parent.left; right: parent.right }
                visible: isMms || isCombo

                SettingsItemTitle {
                    anchors { left: parent.left; right: parent.right }
                    text: I18nd.tr("Proxy")
                }

                LocalComponents.LabelTextField {
                    id: messageProxy
                    enabled: state != "busy"
                    objectName: "messageProxy"
                    inputMethodHints: Qt.ImhUrlCharactersOnly |
                                      Qt.ImhNoAutoUppercase |
                                      Qt.ImhNoPredictiveText
                    placeholderText: I18nd.tr("Enter message proxy")
                    next: port
                    onTextChanged: {
                        movePortDelay.running = false;
                        if (text.search(/\:\d+$/) >= 0) {
                            movePortDelay.running = true;
                        }
                    }
                    onActiveFocusChanged: if (activeFocus) Editor.makeMeVisible(
                        messageProxy)
                }

                Timer {
                    id: movePortDelay
                    interval: 1000
                    onTriggered: {

                        function getPort(s) {
                            var match = s.match(/\:(\d+)/);
                            if (match === null) {
                                return null;
                            } else {
                                return match[1];
                            }
                        }

                        var prt = getPort(messageProxy.text);
                        var portIndex = messageProxy.text.indexOf(prt);
                        var textSansPort = messageProxy.text.slice(0, portIndex - 1);

                        if (prt) {
                            messageProxy.text = textSansPort;
                            port.text = prt;
                        }
                    }
                }
            }

            // Proxy port controls
            Column {
                anchors { left: parent.left; right: parent.right }
                visible: isMms || isCombo

                SettingsItemTitle {
                    id: portLabel
                    text: I18nd.tr("Proxy port")
                }

                LocalComponents.LabelTextField {
                    id: port
                    enabled: state != "busy"
                    objectName: "port"
                    maximumLength: 5
                    inputMethodHints: Qt.ImhNoAutoUppercase |
                                      Qt.ImhNoPredictiveText
                    validator: portValidator
                    placeholderText: I18nd.tr("Enter message proxy port")
                    next: username
                    onActiveFocusChanged: if (activeFocus) Editor.makeMeVisible(
                        port)
                }

                RegExpValidator {
                    id: portValidator
                    regExp: /([0-9]{1,4}|[1-5][0-9]{4}|6[0-4][0-9]{3}|65[0-4][0-9]{2}|655[0-2][0-9]|6553[0-5])/
                }
            }

            // Username controls
            Column {
                anchors { left: parent.left; right: parent.right }

                SettingsItemTitle {
                    width: parent.width
                    text: I18nd.tr("User name")
                }

                LocalComponents.LabelTextField {
                    id: username
                    enabled: state != "busy"
                    objectName: "username"
                    inputMethodHints: Qt.ImhNoAutoUppercase |
                                      Qt.ImhNoPredictiveText |
                                      Qt.ImhSensitiveData
                    placeholderText: I18nd.tr("Enter username")
                    next: password
                    onActiveFocusChanged: if (activeFocus) Editor.makeMeVisible(
                        username)
                }
            }

            // Password controls
            Column {
                anchors { left: parent.left; right: parent.right }

                SettingsItemTitle {
                    width: parent.width
                    text: I18nd.tr("Password")
                }

                LocalComponents.LabelTextField {
                    id: password
                    enabled: state != "busy"
                    objectName: "password"
                    width: parent.width
                    echoMode: TextInput.Normal
                    inputMethodHints: Qt.ImhNoAutoUppercase |
                                      Qt.ImhNoPredictiveText |
                                      Qt.ImhSensitiveData
                    placeholderText: I18nd.tr("Enter password")
                    next: name
                    onActiveFocusChanged: if (activeFocus) Editor.makeMeVisible(
                        password)
                }
            }

            // Authentication controls
            Column {
                anchors { left: parent.left; right: parent.right }
                visible: showAllUI

                SettingsItemTitle {
                    width: parent.width
                    text: I18nd.tr("Authentication")
                }

                ListItem.ItemSelector {
                    model: [I18nd.tr("None"),
                            I18nd.tr("PAP or CHAP"),
                            I18nd.tr("PAP only"),
                            I18nd.tr("CHAP only")]
                }
            }

            // Protocol controls
            Column {
                anchors { left: parent.left; right: parent.right }
                visible: showAllUI

                SettingsItemTitle {
                    width: parent.width
                    text: I18nd.tr("Protocol")
                }

                ListItem.ItemSelector {
                    model: [I18nd.tr("IPv4"),
                            I18nd.tr("IPv6"),
                            I18nd.tr("IPv4v6")]
                }
            }
        }
    } // main column holding all controls and buttons

    Timer {
        id: updateContext
        property OfonoContextConnection ctx
        interval: 1500
        onTriggered: {
            Editor.updateContextQML(ctx);
            root.saved();
        }
    }

    LocalComponents.KeyboardRectangle {
        id: keyboard
        anchors.bottom: parent.bottom
        onHeightChanged: {
            if (root.activeItem) {
                Editor.makeMeVisible(root.activeItem);
            }
        }
    }
}
